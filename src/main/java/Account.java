public class Account {

    private int loanAmount; //Сумма кредита
    private int numberOfMonths; //Кол-во месяцев кредита

    public int getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(int loanAmount) {
        if (loanAmount >= 10000){
            this.loanAmount = loanAmount;
        }
        else {
            System.out.println("Минимальная сумма кредита: 10000 руб." + "\n");
        }
    }

    public int getNumberOfMonths() {
        return numberOfMonths;
    }

    public void setNumberOfMonths(int numberOfMonths) {
        this.numberOfMonths = numberOfMonths;
    }
}
