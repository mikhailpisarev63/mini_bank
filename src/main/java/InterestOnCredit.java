import java.time.LocalDate;
import java.time.Month;

public class InterestOnCredit extends Account {

    /**
     * Класс процент по кредиту
     */

    private double interestRate = 0.12;  //Процентная ставка 12% на год
    private int monthOfTheYear = 12;    //Месяцев году

    //Доля месячной ставки
    public double monthlyInterestRate (){
        double monthlyInterestRate = interestRate / monthOfTheYear;

        return monthlyInterestRate;
    }

    //Расчет итоговой суммы
    public int getRateCalculation(){

        //Ежемесячный платеж
        int monthlyPayment =
                (int) (getLoanAmount() * (monthlyInterestRate() + (monthlyInterestRate() / (Math.pow((1 + monthlyInterestRate()), getNumberOfMonths()) - 1))));

        return monthlyPayment;
    }

    //Итоговая сумма к выплате
    public int getTotalAmountToBePaid (){
        int totalAmount = getRateCalculation() * getNumberOfMonths();
        return totalAmount;
    }

    //Сумма переплаты
    public int getOverpaymentAmount(){
        int overpaymentAmount = getTotalAmountToBePaid() - getLoanAmount();
        return overpaymentAmount;
    }

    //Получить календарь платежей
    public String getPaymentCalendar() {

        LocalDate dateStart = LocalDate.now();
        Month month = dateStart.getMonth();

        int payment = 0;

        for (int i = 0; i < getNumberOfMonths(); i++){
            payment++;
            dateStart = dateStart.plusMonths(1);
            month = month.plus(1);
            System.out.println("Месяц: " + payment + " " + month + "\n"
                    +"\t" + "Дата платежа: " + dateStart + "\n"
                    +"\t" + "Сумма: " + getRateCalculation() + " руб.");
        }
        /*int[][] myArrays = new int[getNumberOfMonths()][getRateCalculation()];

        for (int i = 0; i < myArrays.length; i++) {
            System.out.println("Месяц: " + i + " сумма: " + myArrays[i].length + " руб");
        }*/
        return "Кол-во месяцев: " + payment;
    }
}
