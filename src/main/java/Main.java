
import java.util.Scanner;

public class Main extends Account{

    public static void main(String[] args) {

        System.out.println("Введите сумму кредита: ");

        Scanner scanner = new Scanner(System.in);
        int sum = scanner.nextInt();
        System.out.println("Срок кредитования (в месяцах): ");
        int month = scanner.nextInt();

        System.out.println("\n" + "Сумма кредита: " + sum + " на срок: " + month + " месяца" + "\n");

        InterestOnCredit interestOnCredit = new InterestOnCredit();
        interestOnCredit.setLoanAmount(sum);
        interestOnCredit.setNumberOfMonths(month);

        System.out.println("Ежемесячный платеж: " + interestOnCredit.getRateCalculation() + " руб.");
        System.out.println("Итоговая сумма с учетом процентов: " + interestOnCredit.getTotalAmountToBePaid() + " руб.");
        System.out.println("Сумма переплаты: " + interestOnCredit.getOverpaymentAmount() + " руб.");

        System.out.println("Показать календарь платежей? Да/Нет");
        String calendar = scanner.next();
        if (calendar.equalsIgnoreCase("Да") || calendar.equalsIgnoreCase("Yes"))
        {
            System.out.println(interestOnCredit.getPaymentCalendar());
        }
        else
        {
            System.out.println("Хорошего дня! =)");
        }

        scanner.close();
    }
}
